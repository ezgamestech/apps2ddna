package tech.ezgames.apps2ddna

/**
 * Created by idan on 18 Sep 2016.
 */
interface SqsClient {
    def sendMessage(String sqsUrl, String message)
}