package tech.ezgames.apps2ddna

import com.amazonaws.services.sqs.AmazonSQSAsyncClient
import org.springframework.stereotype.Service
/**
 * Created by idan on 18 Sep 2016.
 */
@Service
class AsyncSqsClient implements SqsClient {

    private final AmazonSQSAsyncClient sqsClient = new AmazonSQSAsyncClient()

    @Override
    def sendMessage(String sqsUrl, String message) {
        sqsClient.sendMessageAsync(sqsUrl, message)
    }
}
