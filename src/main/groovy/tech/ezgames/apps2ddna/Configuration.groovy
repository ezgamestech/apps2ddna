package tech.ezgames.apps2ddna

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

/**
 * Created by idan on 28 Jul 2016.
 */
@Component
class Configuration {
    @Value ('${ez.aws.sqs.audit:#{null}}')
    String sqsAuditUrl

    @Value ('${ez.aws.sqs.deltadna:#{null}}')
    String sqsDdnaUrl
}
