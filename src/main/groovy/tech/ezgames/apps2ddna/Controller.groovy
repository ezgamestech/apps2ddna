package tech.ezgames.apps2ddna

import groovy.json.JsonOutput
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

/**
 * Receives pushes from AppsFlyer. Should push to DeltaDNA
 *
 * Created by idan on 27 Jul 2016.
 */
@RestController
class Controller {

    private static final Logger log = Logger.getLogger(Controller)

    @Autowired
    Configuration configuration

    @Autowired
    SqsClient sqsClient

    ExecutorService executorService = Executors.newSingleThreadExecutor()

    @RequestMapping (path = '/receive', method = RequestMethod.POST)
    public ResponseEntity<Map> receive(@RequestBody Map afEventMap) {

        log.info("Received afEventMap=$afEventMap")

        String auditMsgJson = getAuditMsgJson(afEventMap)
        sqsClient.sendMessage(configuration.sqsAuditUrl, auditMsgJson)
        log.info("Sent to $configuration.sqsAuditUrl message $auditMsgJson")

        if (afEventMap.event_type == 'install') {
            String json = getAfEventJson(afEventMap)
            sqsClient.sendMessage(configuration.sqsDdnaUrl, json)
            log.info("Sent to $configuration.sqsDdnaUrl message $json")
        } else {
            log.info("Skipping sending to DeltaDNA because event_type is not 'install'");
        }

        return ResponseEntity.ok().body(afEventMap)
    }

    private String getAfEventJson(Map afMessageMap) {
        JsonOutput.toJson(afMessageMap)
    }

    private String getAuditMsgJson(Map body) {
        String auditMsgJson = JsonOutput.toJson([
                type   : 'afevent',
                version: 1,
                body   : body
        ])
        auditMsgJson
    }


}
