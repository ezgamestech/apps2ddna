package tech.ezgames.apps2ddna

import spock.lang.Specification
/**
 * Created by idan on 26 Oct 2016.
 */
class ControllerSpec extends Specification {
    static final String DDNA = 'ddna'
    static final String AUDIT = 'audit'

    Configuration cfg = new Configuration(sqsDdnaUrl: DDNA, sqsAuditUrl: AUDIT)
    SqsClient sqsClient = Mock()

    def "Should not send to DDNA"() {
        given:
        Controller ctrl = new Controller(configuration: cfg, sqsClient: sqsClient)

        when:
        ctrl.receive([event_type: 'in-app-event', time: new Date()])

        then:
        1 * sqsClient.sendMessage(AUDIT, _ as String)
        0 * sqsClient.sendMessage(DDNA, _ as String)
    }

    def "Should send to DDNA"() {
        given:
        Controller ctrl = new Controller(configuration: cfg, sqsClient: sqsClient)

        when:
        ctrl.receive([event_type: 'install', time: new Date()])

        then:
        1 * sqsClient.sendMessage(AUDIT, _ as String)
        1 * sqsClient.sendMessage(DDNA, _ as String)
    }
}
